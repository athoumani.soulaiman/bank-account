package com.sgcib.account.utils;

import com.sgcib.account.model.transaction.OperationType;
import com.sgcib.account.model.transaction.Transaction;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StatementFormatterTest {
    public static final String END_LINE_SEPARATOR = System.lineSeparator();

    @Test
    void shouldFormatWhenExistingTransactionsReturnStatementFormatted() {
        long accountId = 0001L;
        LocalDateTime now = LocalDateTime.now();
        var transaction1 = new Transaction(accountId, BigDecimal.ONE, OperationType.DEPOSIT, BigDecimal.ONE, now);
        var transaction2 = new Transaction(accountId, BigDecimal.ONE, OperationType.WITHDRAWAL, BigDecimal.valueOf(2L), now);
        final var transactions = List.of(
                transaction1,
                transaction2
        );

        final var formattedStatement = StatementFormatter.format(transactions);

        var expectedDate = now.format(DateTimeFormatter.ISO_DATE);
        final var expected = List.of(
                "Date;Operation;Amount;CurrentBalance",
                String.format("%s;DEPOSIT;1.00;1.00", expectedDate),
                String.format("%s;WITHDRAWAL;1.00;2.00", expectedDate)
        ).stream().collect(Collectors.joining(END_LINE_SEPARATOR));

        assertEquals(expected, formattedStatement);
    }

    @Test
    void shouldFormatWhenNoTransactionReturnStatementHeader() {
        final var formattedStatement = StatementFormatter.format(List.of());
        assertEquals(StatementFormatter.getHeader(), formattedStatement);
    }

}