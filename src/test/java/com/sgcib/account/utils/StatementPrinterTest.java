package com.sgcib.account.utils;

import com.sgcib.account.model.transaction.Transaction;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StatementPrinterTest {
    @Test
    void shouldFormatAndPrintWhenEmptyListReturnStatementHeader() {
        final var outputStream = new ByteArrayOutputStream();
        final var printStream = new PrintStream(outputStream);
        final var statementPrinter = new StreamStatementPrinter(printStream);
        List<Transaction> transactions = List.of();

        statementPrinter.print(transactions);

        assertEquals(StatementFormatter.getHeader(), outputStream.toString());
    }

}