package com.sgcib.account.repository;

import com.sgcib.account.model.transaction.Transaction;

import java.util.*;

public class InMemoryTransactionRepository implements TransactionRepository {
    private static final Map<Long, List<Transaction>> transactions = new HashMap<>();

    @Override
    public Optional<Transaction> findLast(final long accountId) {
        return transactions.getOrDefault(accountId, new ArrayList<>())
                .stream()
                .reduce((first, second) -> second);
    }

    @Override
    public Transaction save(Transaction transaction) {
        transactions.putIfAbsent(transaction.getAccountId(), new ArrayList<>());
        transactions.computeIfPresent(transaction.getAccountId(), (accountId, oldTransactions) -> {
            final var newTransactions = new ArrayList<>(oldTransactions);
            newTransactions.add(transaction);
            return newTransactions;
        });
        return null;
    }

    @Override
    public List<Transaction> findAll(long accountId) {
        return transactions.get(accountId);
    }

}
