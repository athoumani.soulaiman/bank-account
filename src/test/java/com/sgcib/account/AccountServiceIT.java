package com.sgcib.account;

import com.sgcib.account.model.transaction.OperationType;
import com.sgcib.account.repository.InMemoryTransactionRepository;
import com.sgcib.account.repository.TransactionRepository;
import com.sgcib.account.utils.StatementPrinter;
import com.sgcib.account.utils.StreamStatementPrinter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.*;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("AccountService End to end test")
public class AccountServiceIT {

    private final LocalDateTime fixedDate = LocalDateTime.of(LocalDate.of(2021, 10, 11), LocalTime.of(11, 0));
    private final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    private final StatementPrinter printer = new StreamStatementPrinter(new PrintStream(byteArrayOutputStream));
    private final TransactionRepository transactionRepository = new InMemoryTransactionRepository();

    private final AccountService sut = new DefaultAccountService(transactionRepository, printer, Clock.fixed(fixedDate.toInstant(ZoneOffset.UTC), ZoneOffset.UTC));

    @Test
    void ShouldMakeOperationsWithdrawalsDepositsAndPrintTransactions() throws Exception {
        final var accountId = 1L;
        sut.makeTransaction(accountId, BigDecimal.valueOf(200L), OperationType.DEPOSIT);
        sut.makeTransaction(accountId, BigDecimal.valueOf(10L), OperationType.WITHDRAWAL);
        sut.makeTransaction(accountId, BigDecimal.valueOf(124L), OperationType.DEPOSIT);
        sut.makeTransaction(accountId, BigDecimal.valueOf(11L), OperationType.WITHDRAWAL);
        sut.printStatement(accountId);

        final var expected = List.of(
                "Date;Operation;Amount;CurrentBalance",
                "2021-10-11;DEPOSIT;200.00;200.00",
                "2021-10-11;WITHDRAWAL;10.00;190.00",
                "2021-10-11;DEPOSIT;124.00;314.00",
                "2021-10-11;WITHDRAWAL;11.00;303.00"
        ).stream().collect(Collectors.joining(System.lineSeparator()));

        Assertions.assertEquals(expected, byteArrayOutputStream.toString());
    }
}
