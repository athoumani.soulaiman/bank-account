package com.sgcib.account;

import com.sgcib.account.exception.InvalidAmountException;
import com.sgcib.account.exception.NotEnoughFundsException;
import com.sgcib.account.model.transaction.OperationType;
import com.sgcib.account.model.transaction.Transaction;
import com.sgcib.account.repository.TransactionRepository;
import com.sgcib.account.utils.StatementPrinter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.*;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DefaultAccountServiceTest {

    private final LocalDateTime fixedDate = LocalDateTime.of(LocalDate.of(2021, 10, 11), LocalTime.of(11, 0));

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private StatementPrinter statementPrinter;

    private AccountService sut;

    @BeforeEach
    void init() {
        sut = new DefaultAccountService(transactionRepository, statementPrinter, Clock.fixed(fixedDate.toInstant(ZoneOffset.UTC), ZoneOffset.UTC));
    }

    @Test
    void shouldMakeDepositWhenFirstTransactionReturnCurrentBalanceEqualToDepositAmount() throws Exception {
        long accountId = 1L;
        var amount = BigDecimal.TEN;
        OperationType deposit = OperationType.DEPOSIT;

        Transaction transaction = sut.makeTransaction(accountId, amount, OperationType.DEPOSIT);

        Assertions.assertEquals(amount, transaction.getAmount());
        Assertions.assertEquals(deposit, transaction.getOperationType());
        Assertions.assertEquals(accountId, transaction.getAccountId());
        Assertions.assertEquals(amount, transaction.getCurrentBalance());
        Assertions.assertEquals(fixedDate, transaction.getCreateDate());
    }

    @Test
    void shouldMakeDepositWhenExistingTransactionsReturnAddAmountToCurrentBalance() throws Exception {
        long accountId = 0001L;
        var amount = BigDecimal.TEN;
        var oldBalance = BigDecimal.ONE;
        OperationType deposit = OperationType.DEPOSIT;
        var lastTransaction = new Transaction(accountId, oldBalance, OperationType.DEPOSIT, oldBalance, LocalDateTime.now());
        when(transactionRepository.findLast(accountId)).thenReturn(Optional.of(lastTransaction));

        Transaction transaction = sut.makeTransaction(accountId, amount, deposit);

        Assertions.assertEquals(amount, transaction.getAmount());
        Assertions.assertEquals(deposit, transaction.getOperationType());
        Assertions.assertEquals(accountId, transaction.getAccountId());
        Assertions.assertEquals(oldBalance.add(amount), transaction.getCurrentBalance());
        Assertions.assertEquals(fixedDate, transaction.getCreateDate());
    }

    @Test
    void shouldMakeWithdrawalWhenExistingTransactionsReturnSubtractWithdrawalAmountToCurrentBalance() throws Exception {
        long accountId = 0001L;
        var oldBalance = BigDecimal.TEN;
        var amount = BigDecimal.ONE;
        OperationType withdrawal = OperationType.WITHDRAWAL;
        var lastTransaction = new Transaction(accountId, oldBalance, OperationType.DEPOSIT, oldBalance, LocalDateTime.now());
        when(transactionRepository.findLast(accountId)).thenReturn(Optional.of(lastTransaction));

        Transaction transaction = sut.makeTransaction(accountId, amount, withdrawal);

        Assertions.assertEquals(amount, transaction.getAmount());
        Assertions.assertEquals(withdrawal, transaction.getOperationType());
        Assertions.assertEquals(accountId, transaction.getAccountId());
        Assertions.assertEquals(oldBalance.subtract(amount), transaction.getCurrentBalance());
        Assertions.assertEquals(fixedDate, transaction.getCreateDate());
    }

    @Test
    void shouldMakeWithdrawalWhenNotEnoughMoneyThrowNotEnoughFundsException() {
        long accountId = 0001L;
        var oldBalance = BigDecimal.ONE;
        var amount = BigDecimal.TEN;
        OperationType withdrawal = OperationType.WITHDRAWAL;
        var lastTransaction = new Transaction(accountId, oldBalance, OperationType.DEPOSIT, oldBalance, LocalDateTime.now());
        when(transactionRepository.findLast(accountId)).thenReturn(Optional.of(lastTransaction));

        Executable executable = () -> sut.makeTransaction(accountId, amount, withdrawal);

        Assertions.assertThrows(NotEnoughFundsException.class, executable);
    }

    @Test
    void shouldMakeTransactionWhenNegativeAmountThrowInvalidAmountException() {
        long accountId = 0001L;
        var negativeAmount = BigDecimal.valueOf(-1L);
        OperationType withdrawal = OperationType.WITHDRAWAL;

        Executable executable = () -> sut.makeTransaction(accountId, negativeAmount, withdrawal);

        Assertions.assertThrows(InvalidAmountException.class, executable);
    }

    @Test
    void shouldPrintStatementWhenExistingTransaction() {
        long accountId = 0001L;
        var transaction = new Transaction(accountId, BigDecimal.ONE, OperationType.DEPOSIT, BigDecimal.ONE, LocalDateTime.now());
        List<Transaction> transactions = List.of(transaction);
        when(transactionRepository.findAll(accountId)).thenReturn(transactions);

        sut.printStatement(accountId);

        InOrder inOrder = inOrder(transactionRepository, statementPrinter);
        inOrder.verify(transactionRepository).findAll(accountId);
        inOrder.verify(statementPrinter).print(transactions);
        inOrder.verifyNoMoreInteractions();
    }


}