package com.sgcib.account.repository;

import com.sgcib.account.model.transaction.Transaction;

import java.util.List;
import java.util.Optional;

public interface TransactionRepository {
    Optional<Transaction> findLast(final long accountId);

    Transaction save(Transaction transaction);

    public List<Transaction> findAll(long accountId);
}
