package com.sgcib.account.utils;

import com.sgcib.account.model.transaction.Transaction;
import lombok.AllArgsConstructor;

import java.io.PrintStream;
import java.util.List;

@AllArgsConstructor
public class StreamStatementPrinter implements StatementPrinter {
    private final PrintStream printStream;

    @Override
    public void print(final List<Transaction> transactions) {
        String statement = StatementFormatter.format(transactions);
        printStream.print(statement);
    }
}
