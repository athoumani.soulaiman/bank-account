package com.sgcib.account.utils;

import com.sgcib.account.model.transaction.Transaction;

import java.util.List;

public interface StatementPrinter {
    void print(final List<Transaction> transactions);
}
