package com.sgcib.account.utils;

import com.sgcib.account.model.transaction.Transaction;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class StatementFormatter {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ISO_DATE;
    private static final String FIELD_SEPARATOR = ";";
    private static final String END_LINE_SEPARATOR = System.lineSeparator();

    private StatementFormatter() {
    }

    public static String format(List<Transaction> transactions) {
        return getHeader() +
                transactions.stream().map(StatementFormatter::formatLine).collect(Collectors.joining(END_LINE_SEPARATOR));
    }

    public static String getHeader() {
        StringJoiner stringJoiner = new StringJoiner(FIELD_SEPARATOR);
        stringJoiner.add("Date")
                .add("Operation")
                .add("Amount")
                .add("CurrentBalance");
        return stringJoiner + END_LINE_SEPARATOR;
    }

    private static String formatLine(final Transaction transaction) {
        StringJoiner stringJoiner = new StringJoiner(FIELD_SEPARATOR);
        stringJoiner
                .add(transaction.getCreateDate().format(DATE_TIME_FORMATTER))
                .add(transaction.getOperationType().name())
                .add(formatAmount(transaction.getAmount()))
                .add(formatAmount(transaction.getCurrentBalance()));
        return stringJoiner.toString();
    }

    private static String formatAmount(BigDecimal amount) {
        return String.format("%.2f", amount);
    }

}
