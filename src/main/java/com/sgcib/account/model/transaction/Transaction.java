package com.sgcib.account.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@Builder
public final class Transaction {
    private final Long accountId;
    private final BigDecimal amount;
    private final OperationType operationType;
    private final BigDecimal currentBalance;
    private final LocalDateTime createDate;
}
