package com.sgcib.account.model.transaction;

public enum OperationType {
    DEPOSIT, WITHDRAWAL
}
