package com.sgcib.account;

import com.sgcib.account.exception.InvalidAmountException;
import com.sgcib.account.exception.NotEnoughFundsException;
import com.sgcib.account.model.transaction.OperationType;
import com.sgcib.account.model.transaction.Transaction;
import com.sgcib.account.repository.TransactionRepository;
import com.sgcib.account.utils.StatementPrinter;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
public class DefaultAccountService implements AccountService {

    private TransactionRepository transactionRepository;

    private StatementPrinter statementPrinter;

    private final Clock clock;

    @Override
    public Transaction makeTransaction(Long accountId, final BigDecimal amount, OperationType operationType) throws NotEnoughFundsException, InvalidAmountException {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new InvalidAmountException("Negative amount");
        }

        var oldBalance = transactionRepository.findLast(accountId).map(Transaction::getCurrentBalance).orElse(BigDecimal.ZERO);
        var newBalance = calculateNewBalance(amount, operationType, oldBalance);

        var transaction = Transaction.builder()
                .accountId(accountId)
                .amount(amount)
                .currentBalance(newBalance)
                .operationType(operationType)
                .createDate(LocalDateTime.now(clock)).build();

        transactionRepository.save(transaction);
        return transaction;
    }

    @Override
    public void printStatement(long accountId) {
        List<Transaction> transactions = transactionRepository.findAll(accountId);
        statementPrinter.print(transactions);
    }

    private BigDecimal calculateNewBalance(BigDecimal amount, OperationType type, BigDecimal oldBalance) throws NotEnoughFundsException {
        var newBalance = OperationType.DEPOSIT.equals(type) ? oldBalance.add(amount) : oldBalance.subtract(amount);
        if (newBalance.compareTo(BigDecimal.ZERO) < 0) {
            throw new NotEnoughFundsException("not enough funds");
        }
        return newBalance;
    }


}
