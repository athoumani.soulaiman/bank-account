package com.sgcib.account;

import com.sgcib.account.exception.InvalidAmountException;
import com.sgcib.account.exception.NotEnoughFundsException;
import com.sgcib.account.model.transaction.OperationType;
import com.sgcib.account.model.transaction.Transaction;

import java.math.BigDecimal;

public interface AccountService {

    Transaction makeTransaction(Long accountId, final BigDecimal amount, OperationType operationType) throws NotEnoughFundsException, InvalidAmountException;

    void printStatement(long accountId);

}
